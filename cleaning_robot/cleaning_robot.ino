#define BUTTON_3H A0
#define BUTTON_5H A1
#define BUTTON_ON A2
#define BUTTON_OFF A3

#define LED_3H A4
#define LED_5H A5
#define LED_ON A6
#define LED_OFF A7

#define LED_30S 12
#define LED_60S 11
#define LED_90S 10
#define LED_120S 9

#define RELAIS_FORWARD 8
#define RELAIS_BACKWARD 7
#define RELAIS_PUMP 6

#define TICK_TIME 1000 //ms
#define DIR_CHANGE_TICKS 1 //ms

#define LED_STATE_ON HIGH
#define LED_STATE_OFF LOW

#define BUTTON_STATE_ON HIGH
#define BUTTON_STATE_OFF LOW

#define BUTTON_INPUT_TYPE INPUT

#define S_TO_TICKS(s) s/(TICK_TIME/1000)

#define DEBUG

typedef enum machine_state {OFF, S30, S60, S90, S120} state; 
typedef enum machine_abs_time {H5,H3} dur; 

state s = OFF;
dur d = H3;
uint16_t ticks_iter = 0;
uint16_t abs_ticks = S_TO_TICKS(3*3600);
uint16_t loc_ticks = 0;

 
void setup() {
  #ifdef DEBUG
    Serial.begin(2000000);
  #endif
  pinMode(BUTTON_3H, BUTTON_INPUT_TYPE);
  pinMode(BUTTON_5H, BUTTON_INPUT_TYPE);
  pinMode(BUTTON_ON, BUTTON_INPUT_TYPE);
  pinMode(BUTTON_OFF, BUTTON_INPUT_TYPE);
  pinMode(LED_3H, OUTPUT);
  pinMode(LED_5H, OUTPUT);
  pinMode(LED_ON, OUTPUT);
  pinMode(LED_OFF, OUTPUT);
  pinMode(LED_30S, OUTPUT);
  pinMode(LED_60S, OUTPUT);
  pinMode(LED_90S, OUTPUT);
  pinMode(LED_120S, OUTPUT);
  pinMode(RELAIS_FORWARD, OUTPUT);
  pinMode(RELAIS_BACKWARD, OUTPUT);
  pinMode(RELAIS_PUMP, OUTPUT);
  digitalWrite(LED_3H, LED_STATE_ON);
  digitalWrite(LED_OFF, LED_STATE_ON);
  digitalWrite(LED_5H, LED_STATE_OFF);
  digitalWrite(LED_ON, LED_STATE_OFF);
  digitalWrite(LED_30S, LED_STATE_OFF);
  digitalWrite(LED_60S, LED_STATE_OFF);
  digitalWrite(LED_90S, LED_STATE_OFF);
  digitalWrite(LED_120S, LED_STATE_OFF);
  motor_off();
  pump_off();
}

void loop() {
  
  if (digitalRead(BUTTON_OFF) == BUTTON_STATE_ON)
  {
    d = H3;
    s = OFF;
    //motor_off();
    //pump_off();
  }
  if (s == OFF)
  {
    if (digitalRead(BUTTON_5H) == BUTTON_STATE_ON)
    {
      d = H5;
      digitalWrite(LED_5H, LED_STATE_ON);
      digitalWrite(LED_3H, LED_STATE_OFF);
    }
    if (digitalRead(BUTTON_3H) == BUTTON_STATE_ON)
    {
      d = H3;
      digitalWrite(LED_3H, LED_STATE_ON);
      digitalWrite(LED_5H, LED_STATE_OFF);
    }
    if (digitalRead(BUTTON_ON) == BUTTON_STATE_ON)
    {
      if(d == H3)
      {
        abs_ticks = S_TO_TICKS(3*3600);
      } 
      if(d == H5)
      {
        abs_ticks = S_TO_TICKS(5*3600);
      } 
      s = S30;
      digitalWrite(LED_30S, LED_STATE_ON);
      digitalWrite(LED_ON, LED_STATE_ON);
      digitalWrite(LED_OFF, LED_STATE_OFF);
      loc_ticks = 0;
      motor_forward();
      pump_on();
    }
  }
  if (s == S30)
  {
    if(loc_ticks >= S_TO_TICKS(30))
    {
      s = S60;
      digitalWrite(LED_60S, LED_STATE_ON);
      digitalWrite(LED_30S, LED_STATE_OFF);
      loc_ticks = 0;
      motor_backward();
    }
    else
    {
      loc_ticks++;
    }
  }
  if (s == S60)
  {
    if(loc_ticks >= S_TO_TICKS(60))
    {
      s = S90;
      digitalWrite(LED_90S, LED_STATE_ON);
      digitalWrite(LED_60S, LED_STATE_OFF);
      loc_ticks = 0;
      motor_forward();
      
    }
    else
    {
      loc_ticks++;
    }
  }
  if (s == S90)
  {
    if(loc_ticks >= S_TO_TICKS(90))
    {
      s = S120;
      digitalWrite(LED_120S, LED_STATE_ON);
      digitalWrite(LED_90S, LED_STATE_OFF);
      loc_ticks = 0;
      motor_backward();
    }
    else
    {
      loc_ticks++;
    }
  }
  if (s == S120)
  {
    if(loc_ticks >= S_TO_TICKS(120))
    {
      s = S30;
      digitalWrite(LED_30S, LED_STATE_ON);
      digitalWrite(LED_120S, LED_STATE_OFF);
      loc_ticks = 0;
      motor_forward();
    }
    else
    {
      loc_ticks++;
    }
  }
  if(ticks_iter >= abs_ticks)
  {
    s = OFF;
    digitalWrite(LED_120S, LED_STATE_OFF);
    digitalWrite(LED_90S, LED_STATE_OFF);
    digitalWrite(LED_60S, LED_STATE_OFF);
    digitalWrite(LED_30S, LED_STATE_OFF);
    digitalWrite(LED_ON, LED_STATE_OFF);
    digitalWrite(LED_OFF, LED_STATE_ON);
    motor_off();
    pump_off();
    ticks_iter = 0;
  }
  else if (s != OFF)
  {
    ticks_iter++;
  }
  delay(TICK_TIME);
}

void motor_forward()
{
  digitalWrite(RELAIS_BACKWARD, LED_STATE_OFF);
  delay(TICK_TIME*DIR_CHANGE_TICKS);
  digitalWrite(RELAIS_FORWARD, LED_STATE_ON);
  ticks_iter += DIR_CHANGE_TICKS;
  #ifdef DEBUG
    Serial.print("MOTOR FW\n");
  #endif
}


void motor_backward()
{
  digitalWrite(RELAIS_FORWARD, LED_STATE_OFF);
  delay(TICK_TIME*DIR_CHANGE_TICKS);
  digitalWrite(RELAIS_BACKWARD, LED_STATE_ON);
  ticks_iter += DIR_CHANGE_TICKS;
  #ifdef DEBUG
    Serial.print("MOTOR BW\n");
  #endif
}


void motor_off()
{
  digitalWrite(RELAIS_FORWARD, LED_STATE_OFF);
  digitalWrite(RELAIS_BACKWARD, LED_STATE_OFF);
  #ifdef DEBUG
    Serial.print("MOTOR OFF\n");
  #endif
}

void pump_on()
{
  digitalWrite(RELAIS_PUMP, LED_STATE_ON);
  #ifdef DEBUG
    Serial.print("PUMP ON\n");
  #endif
}

void pump_off()
{
  digitalWrite(RELAIS_PUMP, LED_STATE_OFF);
  #ifdef DEBUG
    Serial.print("PUMP OFF\n");
  #endif
}
